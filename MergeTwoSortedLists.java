/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode dummy = new ListNode(); //dummy node
        ListNode head = dummy; // reference to the ListNode object that we will be returning 
        while (l1 != null && l2 != null) // while i have not reached the end of either list                                                 // either list
        {
            if (l1.val < l2.val)
            {
                dummy.next = l1;
                l1 = l1.next; //advance in the list with the smaller element
            }
            else
            {
                dummy.next = l2;
                l2 = l2.next;
            }
            dummy = dummy.next;
        }
        
        if (l1 != null)
        {
            dummy.next = l1; // once all of the elements of a list are traversed, add the remaining elements from the other list
        }
        else
        {
            dummy.next = l2;
        }
        
        return head.next; // head.next  to disclude the initial ListNode element from the dummy declaration
    }
}